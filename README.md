All code in this project can be used freely without any implications,
provided that you credit me in your work as well.

Currently the code loads  MD5 models along with their animations
and textures.

To load a model we simply create a model object;
/**** example code ****/

MD5Model* model;

model = new Model();
model->loadModel("path/to/your/model");
model->loadAnimation("path/to/models/animation");

/**** END ****/

currently the model and its textures need to be stored in the same 
directory I will try to change this as soon as possible as well.

Note that this code will only load the model, you would still need 
to load your shaders before rendering the model.

To animate the model you will need to setup a render/game loop
and call:

model->update(deltatime);

to update the model.

and then call:

model->render();

in your openGL render callback;

To find out what data your shaders need to contain take a look 
at the prepareMesh function in MD5Model.C