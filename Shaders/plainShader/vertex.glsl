#version 420

layout (location=0) in vec3 position;
layout (location=1) in vec3 normal;
layout (location=2) in vec2 texCoords;

uniform mat4 world;
uniform mat4 view;
uniform mat4 projection;

out vec2 texCo;

void main()
{
    texCo = vec2(texCoords.x, texCoords.y);
	gl_Position = projection * view * world * vec4(position, 1.0);
}
