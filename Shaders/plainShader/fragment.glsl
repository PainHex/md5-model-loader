#version 430
layout(binding = 0) uniform sampler2D textureSample;

in vec2 texCo;
out vec4 f_colour;

void main()
{
	f_colour = texture(textureSample, texCo);
}
