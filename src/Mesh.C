#include "Constants.h"
#include "Mesh.h"

Mesh::
Mesh():
   _vertexCount(0),
   _vao(0)
{
   glGenVertexArrays(1, &_vao);
   id = 0;
   bind();
   scale = vec3(1.0f);
}

Mesh::
~Mesh()
{
   glDeleteVertexArrays(1, &_vao);
}

void Mesh::
draw()
{
    bind();
    glDrawArrays(GL_TRIANGLES, 0, _vertexCount);
}

void Mesh::drawTris()
{
	bind();
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void Mesh::bigBind()
{

}

void Mesh::
bind()
{
   glBindVertexArray(_vao);
}

void Mesh::destroyMesh()
{

}

GLuint Mesh::
findAttribute(const char* name)
{
   GLint currentProgram;
   glGetIntegerv(GL_CURRENT_PROGRAM, &currentProgram);

   GLint location = glGetAttribLocation((GLuint)currentProgram, name);

   if (location == -1)
   {
      string sName(name);
      cerr << "[WARNING] Could not find attribute '" + sName + "'." << endl;
   }

   return (GLuint)location;
}

void Mesh::setOrigin(vec3 newOrigin)
{
    origin = newOrigin;
}

vec3 Mesh::getOrigin()
{
    return origin;
}

void Mesh::drawNormals()
{

}

void Mesh::finalizeMesh()
{

}

unsigned int Mesh::addVertexToMesh(vec3 one, vec3 two)
{

}

void Mesh::addTriangleToMesh(unsigned int one, unsigned int two, unsigned int three)
{

}

