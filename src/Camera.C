#include "Camera.h"

Camera::Camera()
{
	/*r = vec3(0.0f, 0.0f, 0.0f);
	u = vec3(0.0f, 1.0f, 0.0f);
	d = vec3(1.0f, 0.0f, 1.0f);
	direction = d - r;

	view = lookAt(r,d,u);

	for(int i =0; i < 256; i++)
	{
		keyStates[i] = false;
	}*/
}

Camera::Camera(vec3 _r, vec3 _u, vec3 _d)
{
    r = _r;
    u = _u;
    d = _d;
    direction = d - r;

	view = lookAt(r,d,u);

	for(int i =0; i < 256; i++)
	{
		keyStates[i] = false;
	}
}

void Camera::rotateCamera(int xDelta, int yDelta, int xMid, int yMid)
{
	const GLfloat X_SCALED = (GLfloat)xDelta / (GLfloat)xMid;
	const GLfloat Y_SCALED = (GLfloat)yDelta / (GLfloat)yMid;

	const GLfloat LEFT_RIGHT_ROT = X_SCALED * ANGLE_DELTA;
	const GLfloat UP_DOWN_ROT = Y_SCALED * ANGLE_DELTA;


        direction = d - r;
        vec3 right = cross(direction, u);
        vec4 temp(direction.x, direction.y, direction.z, 0.0f);

        mat4 identity;
        identity = rotate(identity, UP_DOWN_ROT, right);
        identity = rotate(identity, LEFT_RIGHT_ROT, u) ;

        temp = identity * temp;

	d = r;
	d[0] += temp[0];
	d[1] += temp[1];
	d[2] += temp[2];

	view = lookAt(r,d,u);
}

void Camera::setKey(unsigned char key, bool state)
{
	keyStates[(int)(key)] = state;
}

void Camera::updateCamera()
{
	direction = d - r;
//	if(keyStates[27] == true)
//	{
//		exit(1);
//	}

	if(keyStates['w'] == true)
	{
		vec3 moveDist;
		moveDist[0] = direction[0];
		moveDist[1] = direction[1];
		moveDist[2] = direction[2];

		r = r + (moveDist);
		d = d + moveDist;
		view = lookAt(r,d,u);
	}

	if(keyStates['s'] == true)
	{
		vec3 moveDist;
		moveDist[0] = direction[0];
		moveDist[1] = direction[1];
		moveDist[2] = direction[2];

		r = r - (moveDist);
		d = d - moveDist;
		view = lookAt(r,d,u);
	}

	if(keyStates['a'] == true)
	{
		vec3 right = cross(direction, u);
		/*right[0] /= 10;
		right[1] /= 10;
		right[2] /= 10;*/

		r = r - right;
		d = d - right;
		view = lookAt(r,d,u);
	}

	if(keyStates['d'] == true)
	{
		vec3 right = cross(direction, u);
		/*right[0] /= 10;
		right[1] /= 10;
		right[2] /= 10;*/

		r = r + right;
		d = d + right;
		view = lookAt(r,d,u);
	}
}

mat4 Camera::getViewMatrix()
{
	return view;
}


vec3 Camera::getEye()
{
	return r;
}
