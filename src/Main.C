#include "Main.h"

void cleanUp()
{
}

float randomFloat(float max, float min)
{
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = min - max;
    float r = random * diff;
    return max + r;
}

void initTextures()
{
    g_Model.loadModel("data/Pinky/pinky.md5mesh");
    g_Model.loadAnimation("data/Pinky/idle1.md5anim");
    g_Model.update(0.0f);

}


void display()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    mat4 identity;
    mat4 oldWorld = world;
    world = identity;

    switchShader(plainShader);
    //voxelChunk->renderChunk();
    g_Model.render();

	glFlush();
	glutSwapBuffers();
	printFPS();
}

void switchShader(ShaderManager* toSwitchTo)
{
	currentShader = toSwitchTo;
	glUseProgram(currentShader->getHandle());
	glUniformMatrix4fv(currentShader->findUniform("world"), 1, GL_FALSE, value_ptr(world));
	glUniformMatrix4fv(currentShader->findUniform("projection"), 1, GL_FALSE, value_ptr(projection));
    glUniformMatrix4fv(currentShader->findUniform("view"), 1, GL_FALSE, value_ptr(myCamera.getViewMatrix()));
}

float getTime()
{
	return glutGet(GLUT_ELAPSED_TIME)/1000.0f;
}

void idle()
{
	int now = getTime();
	int deltaT = now - last_frame;

	if (deltaT >= TARGET_DELTA_T)
	{
	    myCamera.updateCamera();
		glutPostRedisplay();
	}
}

void printFPS()
{
	int now = glutGet(GLUT_ELAPSED_TIME);
	if (now - last_print > MILLISECONDS_PER_SECOND)
	{
		int deltaT = now - last_frame;
		double seconds = (double)deltaT / MILLISECONDS_PER_SECOND;
		double fps = 1.0 / seconds;

		std::ostringstream s;
        s << fps;
        std::string output = "MD5 Loader " + s.str();
        const char* inCharPointerFormat = output.c_str();
		glutSetWindowTitle(inCharPointerFormat);
		last_print = now;

	}
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
void mouseClick(int button, int state, int x, int y)
{
    if(button == 0 && state == 1)
    {
        selectMesh(x, y);
    }
}
#pragma GCC diagnostic pop

void selectMesh(int x, int y)
{

}

void init()
{
	plainShader = new ShaderManager();

	plainShader->attachNewShader("Shaders/PlainShader/vertex.glsl", 0);
	plainShader->attachNewShader("Shaders/PlainShader/fragment.glsl", 1);
	plainShader->linkToShaders();


	glUseProgram(plainShader->getHandle());

	currentShader = plainShader;

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	lightPos = vec3(500.0f, 150.0f, -500.0f);
	lightView = lookAt(lightPos, vec3(500.0f, 0.0f, -500.0f), vec3(0,0,-1));

	last_frame = getTime();
	last_print = getTime();

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	switchShader(plainShader);
	voxelChunk = new VoxelChunk(0.0f, 0.0f, 0.0f);
}

void initGLUT(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_MULTISAMPLE);
    glEnable(GL_MULTISAMPLE);
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    glutCreateWindow("VoxelEngine");
    //glutGameModeString( "1920x1080:16@60" );
    //glutEnterGameMode();
    glutSetCursor(GLUT_CURSOR_NONE);
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutKeyboardUpFunc(keyboardUp);
    glutPassiveMotionFunc(mouse);
    glutMouseFunc(mouseClick);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);


    glutCreateMenu(menu);
    glutAddMenuEntry("Wireframe", WIREFRAME);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void menu(int option)
{
    if(option == WIREFRAME)
    {
        wireFrameMode = !wireFrameMode;
        if(wireFrameMode == true)
        {
            glPolygonMode(GL_FRONT, GL_LINE);
            glPolygonMode(GL_BACK, GL_LINE);
            currentDrawMode = GL_LINE;
        }
        else
        if(wireFrameMode == false)
        {
            glPolygonMode(GL_FRONT, GL_FILL);
            glPolygonMode(GL_BACK, GL_FILL);
            currentDrawMode = GL_FILL;
        }
    }
}

void keyboardUp(unsigned char key, int x, int y)
{
    myCamera.setKey(key, false);
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	    case 27:
	    {
	        cleanUp();
	        exit(1);
	        break;
	    }
		default:
		{
			break;
		}
	}

	myCamera.setKey(key, true);
}
#pragma GCC diagnostic pop

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
void mouse(int x, int y)
{
   	const int X_MID = WINDOW_WIDTH/ 2;
	const int Y_MID = WINDOW_HEIGHT  / 2;

	const int X_DELTA = -(x - X_MID);
	const int Y_DELTA = -(y - Y_MID);

	/*******
	Camera Rotation
	*******/

	myCamera.rotateCamera(X_DELTA, Y_DELTA, X_MID, Y_MID);

	if ((X_DELTA != 0) || (Y_DELTA != 0))
	{
	      glutWarpPointer(X_MID, Y_MID);
	}

}
#pragma GCC diagnostic pop

void reshape(int newWidth, int newHeight)
{
	glViewport(0, 0, newWidth, newHeight);
	GLfloat aspect = (GLfloat)newWidth / (GLfloat)newHeight;
	projection = perspective(90.0f, aspect, 0.2f, 1500.0f);

	glutPostRedisplay();
}

int
main(int argc, char** argv)
{
	initGLUT(argc, argv);
	glewInit ();
	//glutFullScreen();
	if (! GLEW_VERSION_4_2 )
	{
		fprintf (stderr , " This   program   requires   OpenGL   4.2 ");
		//exit ( -1);
	}

    vec3 r(0.0f, 5.0f, 0.0f);
	vec3 u(0.0f, 1.0f, 0.0f);
	vec3 d(1.0f, 5.0f, 1.0f);
	myCamera = Camera(r, u, d);


    srand (time(NULL));

    init();
    initTextures();

	last_frame = getTime();

    while(true)
    {
        float now = getTime();
        float deltaT = now - last_frame;

        if (deltaT >= TARGET_DELTA_T)
        {
            g_Model.update( deltaT );
            glutPostRedisplay();
            last_frame = now;
        }
        myCamera.updateCamera();
        glutMainLoopEvent();
    }

	cleanUp();

	return 0;
}
