#ifndef EDITABLEMESH_H
#define EDITABLEMESH_H

#include <vector>

#include "Mesh.h"

using std::vector;

class EditableMesh : public Mesh
{
    public:
        EditableMesh();
        unsigned int addVertexToMesh(vec3 vertex, vec3 normal);
        void addTriangleToMesh(unsigned int p1, unsigned int p2, unsigned int p3);
        void draw();

        void finalizeMesh();
        void destroyMesh();

        virtual ~EditableMesh();
    protected:
    private:
        std::vector<GLfloat> vertices;
        std::vector<unsigned int> indices;
        std::vector<GLfloat> normals;
        GLuint buffers[3];

};

#endif // EDITABLEMESH_H
