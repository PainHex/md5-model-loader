#ifndef CONSTANTS_H
#define CONSTANTS_H

/* ========== Includes for GLM library and FreeGLUT ======*/

#ifdef __gnu_linux__
    #include <GL/glew.h>
    #include <GL/freeglut.h>
#endif
#ifdef WIN32
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLtools.h>
#define GLAPI extern
#define FREEGLUT_STATIC
#include <GL/freeglut.h>
#endif

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
#pragma GCC diagnostic ignored "-Wconversion"
	#include "glm/glm.hpp"
	#include "glm/gtc/matrix_transform.hpp"
	#include "glm/gtc/type_ptr.hpp"
	#include <glm/gtc/quaternion.hpp>
    #include <glm/gtx/vector_angle.hpp>
    #include <glm/gtx/compatibility.hpp>
    #include <glm/gtx/matrix_operation.hpp>
    #include <glm/gtx/transform.hpp>
    #include <glm/gtx/quaternion.hpp>
    #include <glm/gtx/euler_angles.hpp>
#pragma GCC diagnostic pop

/* ======= END ======= */

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/array.hpp>
#include <boost/asio.hpp>

#include "SOIL.h"

using glm::mat4;
using glm::vec3;
using glm::vec2;
using glm::scale;
using glm::rotate;
using glm::translate;
using glm::value_ptr;
using glm::lookAt;
using glm::perspective;
using glm::cross;
using glm::vec4;
using boost::asio::ip::udp;

#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/fstream.hpp>
namespace fs = boost::filesystem;

/**
  * This file contains constants that are global over the whole project.
  */

/** The value of X, when used as an index. */
static const int X = 0;
/** The value of Y, when used as an index. */
static const int Y = 1;
/** The value of Z, when used as an index. */
static const int Z = 2;
/** The value of W, when used as an index. */
static const int W = 3;

/** Pi. */
static const float PI = 3.141592653589793f;
static const float TWOPI = PI*2.0f;

static const int CHUNK_SIZE = 16;
static const float BLOCK_SIZE = 0.5f;

namespace boost
{
    namespace serialization
    {
        template<class Archive>
        void serialize(Archive & ar, glm::detail::tmat4x4<glm::mediump_float> transform, const unsigned int version)
        {
            ar & transform[0];
            ar & transform[1];
            ar & transform[2];
            ar & transform[3];
        }

        template<class Archive>
        void serialize(Archive & ar, glm::detail::tvec3<glm::mediump_float> vec, const unsigned int version)
        {
            ar & vec.x;
            ar & vec.y;
            ar & vec.z;
        }

        template<class Archive>
        void serialize(Archive & ar, glm::detail::tvec4<glm::mediump_float> vec, const unsigned int version)
        {
            ar & vec.x;
            ar & vec.y;
            ar & vec.z;
            ar & vec.w;
        }
    }
}

#endif

