#ifndef NETWORKMESSAGE_H
#define NETWORKMESSAGE_H

#include <vector>
#include <boost/serialization/vector.hpp>

#include "Constants.h"
#include "Player.h"

class NetworkMessage
{
    public:
        NetworkMessage();
        void addPlayer(Player* player);
        virtual ~NetworkMessage();
    protected:
    private:
        std::vector<Player*> players;

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & players;
        }
};

#endif // NETWORKMESSAGE_H
