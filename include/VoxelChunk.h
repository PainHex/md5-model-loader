#ifndef VOXELCHUNK_H
#define VOXELCHUNK_H

#include "Constants.h"
#include "VoxelPoint.h"
#include "VoxelCube.h"
#include "Mesh.h"
#include "EditableMesh.h"

class VoxelChunk
{
    public:
        VoxelChunk();
        VoxelChunk(GLfloat x, GLfloat y, GLfloat z);

        void renderChunk();
        void generateChunkRenderMesh();

        virtual ~VoxelChunk();
    protected:
    private:
        //Methods
        void initializeChunk();
        void addCubeToMesh(int x, int y, int z);

        //Members
        VoxelPoint*** voxelArray;
        EditableMesh* voxelMesh;
        GLfloat chunkX;
        GLfloat chunkY;
        GLfloat chunkZ;
};

#endif // VOXELCHUNK_H
