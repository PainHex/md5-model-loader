#ifndef MYCAMERA_H
#define MYCAMERA_H

#ifdef __gnu_linux__
    #include <GL/glew.h>
    #include <GL/freeglut.h>
#endif
#ifdef WIN32
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLtools.h>
#define GLAPI extern
#define FREEGLUT_STATIC
#include <GL/freeglut.h>
#endif

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
#pragma GCC diagnostic ignored "-Wconversion"
	#include "glm/glm.hpp"
	#include "glm/gtc/matrix_transform.hpp"
	#include "glm/gtc/type_ptr.hpp"
#pragma GCC diagnostic pop

#include "Constants.h"

using glm::mat4;
using glm::vec3;
using glm::scale;
using glm::rotate;
using glm::translate;
using glm::value_ptr;
using glm::lookAt;
using glm::perspective;
using glm::cross;
using glm::vec4;

class Camera
{
	private:

    mat4 view;
	vec3 r;
	vec3 u;
	vec3 d;
	vec3 direction;
	bool keyStates[256];

    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
        //ar & view;
        ar & r;
        ar & u;
        ar & d;
        //ar & direction;
        ar & keyStates;
    }

    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
        //ar & view;
        ar & r;
        ar & u;
        ar & d;
        //ar & direction;
        ar & keyStates;
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()

	#ifdef __gnu_linux__
		const GLfloat ANGLE_DELTA = 2 * 360.0f / 360.0f * 3.14f;
	#endif

	#ifdef WIN32
		static const GLfloat ANGLE_DELTA = 2 * 360.0f / 360.0f * 3.14f;
	#endif




	public:
	Camera();
	Camera(vec3 r, vec3 u, vec3 d);
	void rotateCamera(int xDelta, int yDelta, int xMid, int yMid);
	void setKey(unsigned char key, bool state);
	void updateCamera();
	mat4 getViewMatrix();
	vec3 getEye();

};
#endif
