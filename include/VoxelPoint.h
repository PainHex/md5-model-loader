#ifndef VOXELPOINT_H
#define VOXELPOINT_H


class VoxelPoint
{
    public:
        VoxelPoint();

        bool isActive();
        void setActive(bool flag);

        virtual ~VoxelPoint();
    protected:
    private:
        bool active;
};

#endif // VOXELPOINT_H
