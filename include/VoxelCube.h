#ifndef VOXELCUBE_H
#define VOXELCUBE_H

#include "Constants.h"

using glm::vec3;

class VoxelCube
{
    public:
        VoxelCube();

        //Draw Method
        static void drawVoxelCube(GLfloat x, GLfloat y, GLfloat z);

        //Draw Method - Location management delegated to caller of function
        static void drawVoxelCube();

        virtual ~VoxelCube();
    protected:
    private:
};

#endif // VOXELCUBE_H
