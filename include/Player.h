#ifndef PLAYER_H
#define PLAYER_H

#include "Constants.h"


class Player
{
    public:
        Player();
        Player(float _x, float _y, float _z, std::string _name);
        virtual ~Player();
    protected:
    private:
        float x;
        float y;
        float z;
        std::string name;

    friend class boost::serialization::access;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
        ar & x;
        ar & y;
        ar & z;
        ar & name;
    }

    template<class Archive>
    void load(Archive & ar, const unsigned int version)
    {
        ar & x;
        ar & y;
        ar & z;
        ar & name;
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

#endif // PLAYER_H
