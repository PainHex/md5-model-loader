#ifndef MESH_H
#define MESH_H

#include <iostream>
#include <string>

#ifdef __gnu_linux__
    #include <GL/glew.h>
    #include <GL/freeglut.h>
#endif
#include "Constants.h"
#ifdef WIN32
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLtools.h>
#define GLAPI extern
#define FREEGLUT_STATIC
#include <GL/freeglut.h>
#endif

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
#pragma GCC diagnostic ignored "-Wconversion"
	#include "glm/glm.hpp"
	#include "glm/gtc/matrix_transform.hpp"
	#include "glm/gtc/type_ptr.hpp"
#pragma GCC diagnostic pop


using std::cerr;
using std::endl;
using std::cout;
using std::string;
using glm::vec3;

class Mesh
{
   public:
      /**
        * Constructor.
        */
      Mesh();

      /**
        * Destructor. Deallocates memory.
        */
      virtual ~Mesh();

      /**
        * Draw the mesh.
        */
      virtual void draw();
      virtual void drawTris();
      virtual void drawNormals();

      virtual void bigBind();
      unsigned int addVertexToMesh(vec3 vertex, vec3 normal);
      void addTriangleToMesh(unsigned int p1, unsigned int p2, unsigned int p3);
      void finalizeMesh();
      void destroyMesh();
      void setOrigin(vec3 newOrigin);
      vec3 getOrigin();
      vec3 scale;

   protected:
      /**
        * Bind to the context.
        */
      void bind();

      /**
        * Helper function to find vertex attribute locations.
        *
        * @param name The attribute's name in the shader source.
        *
        * @return The attribute's location as a GLuint.
        *
        * @throws LinkError If the attribute could not be found.
        */
        GLuint findAttribute(const char* name);
      /**
        * Amount of vertices in this mesh.
        */
        GLsizei _vertexCount;
        int id;


   private:
      /**
        * Handle for the VAO.
        */
      GLuint _vao;
      vec3 origin;
};

#endif
