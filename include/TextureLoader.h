#ifndef TEXTURELOADER_H
#define TEXTURELOADER_H

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <noise/noise.h>
#include "Constants.h"

#ifdef __gnu_linux__
    #include <GL/glew.h>
    #include <GL/freeglut.h>
#endif
#ifdef WIN32
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLtools.h>
#define GLAPI extern
#define FREEGLUT_STATIC
#include <GL/freeglut.h>
#endif

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
#pragma GCC diagnostic ignored "-Wconversion"
	#include "glm/glm.hpp"
	#include "glm/gtc/matrix_transform.hpp"
	#include "glm/gtc/type_ptr.hpp"
#pragma GCC diagnostic pop

#include "lodepng.h"

using noise::module::Perlin;

class TextureLoader
{
    public:
        TextureLoader();
        GLuint loadTexture(const char* path, GLenum textureType, GLenum activeTextureUnit, GLenum wrapMode, GLenum filterMode);
        void loadSkyboxTexture(const char* path, GLenum textureType, GLenum side, GLenum activeTextureUnit, GLenum wrapMode, GLenum filterMode);
        void createPerlinTexture(GLenum activeTextureUnit);
        void bind();
        GLubyte* getData();
        virtual ~TextureLoader();
    protected:
    private:
        GLuint texture;
        GLenum activeTexture;
        const char* imgPath;
        GLubyte* data;
};

#endif // TEXTURELOADER_H
