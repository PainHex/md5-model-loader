#ifndef AABB_H
#define AABB_H

#ifdef __gnu_linux__
    #include <GL/glew.h>
    #include <GL/freefreeglut.h>
#endif
#include "Constants.h"
#ifdef WIN32
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLtools.h>
#define GLAPI extern
#define FREEGLUT_STATIC
#include <GL/freeglut.h>
#endif
#include <vector>



class AABB
{
    public:
        AABB(GLfloat xCentre, GLfloat yCentre, GLfloat zCentre);
        virtual ~AABB();
    protected:
    private:
};

#endif // AABB_H
