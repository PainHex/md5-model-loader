#ifndef SHADERMANAGER_H
#define SHADERMANAGER_H

#include <cmath>
#include <cstdlib>
#include <iostream>

#ifdef __gnu_linux__
    #include <GL/glew.h>
    #include <GL/freeglut.h>
#endif
#include "Constants.h"
#ifdef WIN32
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLtools.h>
#define GLAPI extern
#define FREEGLUT_STATIC
#include <GL/freeglut.h>
#endif

using std::cout;
using std::endl;

class ShaderManager
{
	private:
		GLuint shaderProg;
		GLuint vertHandle;
		GLuint fragHandle;
		GLuint tessEHandle;
		GLuint tessCHandle;
		GLuint geoHandle;

	public:
		ShaderManager();
		void attachNewShader(const char* path, int type);
		void linkToShaders();
		GLuint getHandle();


		GLint findUniform(const char* name);
		char* readShaderProg(const char* fileName);

};
#endif
