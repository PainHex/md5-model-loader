#ifndef MAIN_H
#define MAIN_H


#include <cmath>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>


#ifdef __gnu_linux__
    #include <GL/glew.h>
    #include <GL/freeglut.h>
#endif

#ifdef WIN32
#define GLEW_STATIC
#include <GL/glew.h>
#define GLAPI extern
#define FREEGLUT_STATIC
#include <GL/freeglut.h>
#endif


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
#pragma GCC diagnostic ignored "-Wconversion"
	#include "glm/glm.hpp"
	#include "glm/gtc/matrix_transform.hpp"
	#include "glm/gtc/type_ptr.hpp"
#pragma GCC diagnostic pop

#include <time.h>

#include "ShaderManager.h"
#include "Mesh.h"
#include "Camera.h"
#include "Constants.h"
#include "VoxelChunk.h"
#include "MD5Model.h"


using glm::mat4;
using glm::vec3;
using glm::scale;
using glm::rotate;
using glm::translate;
using glm::value_ptr;
using glm::lookAt;
using glm::perspective;


/** Number of milliseconds in a second. */
const int MILLISECONDS_PER_SECOND = 1000;

/** Character code for the escape key in GLUT. */
const int KEY_ESCAPE = 27;

/** The window's initial width. */
const int WINDOW_WIDTH = 640;
/** The window's initial height. */
const int WINDOW_HEIGHT = 480;
/** The target frames per second. */
const unsigned int TARGET_FPS = 24;
/** The target time interval between frames (in nanoseconds). */
const float TARGET_DELTA_T = (float)1.0f / (float)TARGET_FPS;

enum MENU
{
   WIREFRAME
};

mat4 world;
mat4 view;
mat4 projection;
mat4 worldRot;
mat4 skyWorld;

Camera myCamera;

ShaderManager* currentShader;

bool wireFrameMode = false;

unsigned int clientOrServer = NULL;
GLuint currentDrawMode;

/** Last time a frame was drawn. */
float last_frame;
/** Last time the FPS was printed. */
int last_print;

MD5Model g_Model;

GLint worldLoc;
GLint viewLoc;
GLint projLoc;
GLint lightLoc;

ShaderManager* plainShader;

vec3 lightPos(0.0, 3.0, 0.0);

mat4 lightView;

boost::asio::io_service io_service;

VoxelChunk* voxelChunk;


    void cleanUp();
    inline bool initializeSockets();
    inline void shutdownSockets();
    void display();
    float getTime();
    void idle();
    void init();
    void initGLUT(int argc, char** argv);
    void keyboard(unsigned char key, int x, int y);
    void keyboardUp(unsigned char key, int x, int y);
    void list_hits(GLint hits, GLuint *names);
    void mouse(int x, int y);
    void mouseClick(int button, int state, int x, int y);
    void menu(int option);
    void printFPS();
    void reshape(int newWidth, int newHeight);
    void initTextures();
    void switchShader(ShaderManager* toSwitchTo);
    void selectMesh(int x, int y);
    float randomFloat(float max, float min);

#endif
